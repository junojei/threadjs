import { PostsApiPath } from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit('new_post', post); // notify all users that a new post was created
      return post;
    })
    .put(PostsApiPath.REACT, async req => {
      const reaction = await postService.setReaction(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked or dislike his post
        if (reaction.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }

      return reaction;
    })
    .put(PostsApiPath.$ID, async req => {
      const post = await postService.updateById(req.params.id, req.body);
      return post;
    })
    .delete(PostsApiPath.$ID, req => postService.deleteById(req.params.id));

  done();
};

export { initPost };
