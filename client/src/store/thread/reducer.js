import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  editedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.toggleEditedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.editedPost = post;
  });
  builder.addCase(threadActions.editPost.fulfilled, (state, action) => {
    const { post: updatePost } = action.payload;
    const oldPostIndex = state.posts.findIndex(post => post.id === updatePost.id);

    state.posts[oldPostIndex] = updatePost;
    state.editedPost = state.editedPost ? updatePost : null;
  });
  builder.addCase(threadActions.deletePost.fulfilled, (state, action) => {
    state.posts = state.posts.filter(post => post.id !== action.payload.postId);
    state.expandedPost = null;
  });
  builder.addCase(threadActions.toggleExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.fulfilled,
      threadActions.dislikePost.fulfilled,
      threadActions.addComment.fulfilled
    ),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(isAnyOf(
    threadActions.applyPost.fulfilled,
    threadActions.createPost.fulfilled
  ), (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
});

export { reducer };
