// import PropTypes from 'prop-types';
import { useCallback, useDispatch, useSelector, useState } from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';
import { Spinner, Modal, Button, Image, TextArea } from 'components/common/common';

// import { useCallback, useState } from 'hooks/hooks';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'common/enums/enums';
// import { Button, Image, TextArea, Segment } from 'components/common/common';

import styles from './styles.module.scss';

const EditedPost = ({ onPostEdit, uploadImage }) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.editedPost
  }));
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleTextAreaChange = useCallback(ev => setBody(ev.target.value), [setBody]);

  const handleEditedPostToggle = useCallback(id => (
    dispatch(threadActionCreator.toggleEditedPost(id))
  ), [dispatch]);

  const handleEditedPostClose = () => handleEditedPostToggle();

  const handleEditPost = async () => {
    if (!body) {
      return;
    }

    const updatePost = {
      id: post.id,
      imageId: image ? image.id : null,
      body
    };
    await onPostEdit(updatePost);
    handleEditedPostClose();
  };

  return (
    <Modal
      isOpen
      onClose={handleEditedPostClose}
    >
      {post ? (
        <form onSubmit={handleEditPost}>
          <TextArea
            name="body"
            value={body}
            placeholder="Post will be deleted if you leave this empty"
            onChange={handleTextAreaChange}
          />
          {image?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.imageLink} alt="post image" />
            </div>
          )}
          {image?.link && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.link} alt="post image" />
            </div>
          )}
          <div className={styles.btnWrapper}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Change image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
            <div className={styles.btnWrapper}>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Post
              </Button>
              <Button color={ButtonColor.WHITE} type={ButtonType.SUBMIT}>
                Close
              </Button>
            </div>
          </div>
        </form>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

EditedPost.propTypes = {
  onPostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default EditedPost;
