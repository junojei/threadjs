const ButtonColor = {
  TEAL: 'teal',
  BLUE: 'blue',
  WHITE: 'white'
};

export { ButtonColor };
